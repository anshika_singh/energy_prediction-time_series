In this project I have built a machine learning  model for Time Series Analysis using tidyquant and timetk libraries. The data that I have used is 
hourly data so it tends to be very noisy. Therefore I have used a bunch of time series tools in order to help me in extracting trends and making visualisations easier.
